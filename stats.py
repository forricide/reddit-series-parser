import json
from config import Configuration as conf


def prettyp(d):
    print(json.dumps(d, indent=2))


def top_n(l, n=1, k=lambda x: x["Submission Score"]):
    sl = sorted(l, key=k)
    return sl[-n:]


def list_from(d):
    return [d[k] for k in d]


def top_n_dict(d, n=1, k=lambda x: x["Submission Score"]):
    return top_n(list_from(d), n, k)


def top_n_discussions(k="votes", n=3, path="output/all_discussions.json"):
    with open(path, 'r') as file:
        data = json.load(file)
    if k == "votes":
        return top_n_dict(d=data, n=n, k=lambda x: x["Submission Score"])
    if k == "comments":
        return top_n_dict(d=data, n=n, k=lambda x: x["Number of Comments"])
    if k == "ctov":
        return top_n_dict(d=data, n=n, k=lambda x: (x["Number of Comments"] / x["Submission Score"]))
    if k == "vtoc":
        return top_n_dict(d=data, n=n, k=lambda x: (x["Submission Score"] / x["Number of Comments"]))
    if isinstance(k, str):
        return top_n_dict(d=data, n=n, k=lambda x: x[k])

    return top_n_dict(d=data, n=n, k=k)


def fmt_top_print(t, d, k1, k2):
    print(t)
    for x in reversed(range(0, 3)):
        print(str(round(k1(d[x]), 2)) +
              " " + k2(d[x]) + ":", d[x]['Title'])


def fmt_print(c=conf.default()):
    top_c = top_n_discussions(
        k="comments", path=conf.get("list-file"))
    top_v = top_n_discussions(k="votes")
    top_ctov = top_n_discussions(k="ctov")
    top_vtoc = top_n_discussions(k="vtoc")

    fmt_top_print("Most heavily commented discussions:", top_c,
                  lambda x: x['Number of Comments'], lambda x: "comments")
    fmt_top_print("Most upvoted discussions:", top_v,
                  lambda x: x['Submission Score'], lambda x: "votes")
    fmt_top_print("Highest comment:vote ratio:", top_ctov, lambda x: (x["Number of Comments"] / x["Submission Score"]),
                  lambda x: "comments/vote (" + str(x["Number of Comments"]) + ":" + str(x["Submission Score"]) + ")")
    fmt_top_print("Highest vote:comment ratio:", top_vtoc, lambda x: (x["Submission Score"] / x["Number of Comments"]),
                  lambda x: "votes/comment (" + str(x["Submission Score"]) + ":" + str(x["Number of Comments"]) + ")")


def main(a):
    c = conf.default()
    c.overwrite(filename="config.json")
    for arg in a:
        mo = re.match(r'^-+([^=]+)$', arg)
        if mo is not None:
            c.overwrite(source={mo.group(1): True})
            continue
        mo = re.match(r'^-+([^=]+)=([^=]+)$', arg)
        if mo is not None:
            c.overwrite(source={mo.group(1): mo.group(2)})
            continue
    c.write('Running with config:')
    c.write(c.dump(indent=2))


if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
