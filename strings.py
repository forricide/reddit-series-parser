"""
Provides constant strings. Mostly JSON/Dictionary keys.
"""

class Strings:
    """
    Posts have a "number" that orders them within a series.
    This key corresponds to that value.
    """
    post_number = "post-number"
