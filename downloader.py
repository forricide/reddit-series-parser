"""
Reddit post parsing tools.

For development, see: https://praw.readthedocs.io/en/latest/
"""

from config import Configuration as conf
from strings import Strings as Strs

import praw
import json
import re
import sys
import os
import time


class RedditID:
    def __init__(self, post_id, post_sub=None):
        self.internal_id = post_id
        self.internal_sub = post_sub.strip(
            '/') if post_sub is not None else None

    def set_sub(self, sub):
        self.internal_sub = sub.strip('/')

    def url(self):
        return 'https://reddit.com/' + self.sub() + '/comments/' + self.id()

    def sub(self):
        return self.internal_sub

    def id(self):
        return self.internal_id


def find_next_post(text, c):
    # c.write(c.get(["parsing", "find-prev"]))
    previous_re = re.compile(
        c.get(["parsing", "find-prev"]))
    res = re.search(previous_re, text)
    return res.group(c.get(["parsing", "find-prev-group"], default=3)) if res is not None else None


def find_prev_post(text, c):
    # c.write(c.get(["parsing", "find-next"]))
    next_re = re.compile(c.get(["parsing", "find-next"]))
    res = re.search(next_re, text)
    return res.group(c.get(["parsing", "find-next-group"], default=3)) if res is not None else None


def real_path(path):
    # return os.path.join(os.path.dirname(os.path.realpath(__file__)), '../', path)
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), path)


def get_id_path(kind, reddit_id):
    return real_path('output/' + kind + '_' + reddit_id + '.s.json')


def get_comments_path(reddit_id):
    return get_id_path('comments', reddit_id)


def get_post_path(reddit_id):
    return get_id_path('discussion', reddit_id)


def write_pretty_json(path, data):
    with open(path, 'w') as file:
        json.dump(data, file, indent=4, sort_keys=True)


def get_reddit_instance(c):
    """
    :return: Returns a Reddit instance.
    """
    auth_path = real_path(c.get("auth-path"))
    with open(auth_path, 'r') as file:
        json_data = json.load(file)
    ri = praw.Reddit(client_id=json_data["client_id"],
                     client_secret=json_data["client_secret"],
                     password=json_data["password"],
                     user_agent=json_data["user_agent"],
                     username=json_data["username"])
    return ri


def get_no(string=None):
    """
    Gets user input, checks for a non-positive answer.
    :param string: Optional prompt.
    :return: False if the user answers yes, otherwise True.
    """
    ans = input(string) if string is not None else input()
    if ans.lower() not in ['y', 'yes']:
        return True
    return False


def get_general(source, destination):
    """
    Gets some generic Reddit information from source and puts it into destination.
    :param source: Either a Reddit Submission or Comment.
    :param destination: Any dictionary, keys/values will be added.
    :return: No return, uses destination as an out param.
    """
    destination['reddit-id'] = source.id
    if source.author is not None:
        destination['author'] = source.author.name
    else:  # Occurs when the author deleted their account.
        destination['author'] = "[Deleted]"
    destination['timestamp'] = str(source.created_utc)
    destination['permalink'] = 'https://reddit.com' + \
        source.permalink
    destination['score'] = source.score


def get_comments(submission, c):
    """
    Gets a dictionary containing all comments in a submission.
    :param submission: The Reddit API Submission object.
    :return: A dictionary containing comments.
    """

    c.write("Gathering comments for submission:",
            submission.id)

    # Include a metadata thing for easy information.
    comments = {'meta': {}}

    # Warning: This uses up 1 request for each 'More Comments' that it replaces.
    # This script does not need to execute quickly, so do this slowly.
    submission.comments.replace_more(limit=30)

    # .list() flattens the tree
    submission_comments = submission.comments.list()
    c.write("Found", len(submission_comments), "comments.")
    for submission_comment in submission_comments:
        comment = {}
        try:
            get_general(source=submission_comment,
                        destination=comment)
            comment['fulltext'] = submission_comment.body
            comment['edited'] = submission_comment.edited
            comment['parent'] = submission_comment.parent_id
        except Exception as e:
            err = 'comment was none' if comment is None else (
                'comment(' + str(comment) + ')')
            print(
                'WARNING: get_comments threw an exception:', err)
            print(e)

        if 'reddit-id' in comment:
            comments[comment['reddit-id']] = comment
    comments['meta']['num-comments'] = len(comments) - 1
    return comments


def download(url=None, c=None, rid=None, fout=None, reddit_instance=None):
    """
    Downloads the discussion and comments at `url`.
    Adds basic information for the discussion to config["list-file"]
    Creates two files: comments_[id].s.json and discussion_[id].s.json
    Where [id] is Reddit's submission id (6 alphanumeric characters)
    :param url: URL for the submission.
    :param fout: Mostly for debugging - instead of using Reddit's submission id for [id], uses this param instead.
    :param reddit_instance: For efficient multiple uses, create a Reddit instance outside this method and pass it in.
    :return: Returns the submission's data for easy use.
    """

    if c is None:
        return "Error: Configuration not supplied."
    if url is None and rid is None:
        return "Error: Must supply one of id / url."

    # Get a reddit instance
    if reddit_instance is None:
        reddit_instance = get_reddit_instance(c)

    # We're downloading one specific submission
    if rid is not None:
        s = reddit_instance.submission(id=rid)
    else:
        s = reddit_instance.submission(url=url)

    # First scrape submission metadata
    post_data = {
        'link-flair-text': s.link_flair_text,
        'num-comments': s.num_comments,
        'shortlink': s.shortlink,
        'fulltext': s.selftext, 'title': s.title
    }
    get_general(s, post_data)

    # discussion ID
    reddit_id = fout if fout is not None else post_data['reddit-id']

    # Great! Now that we have that, check to see if
    # this one already exists in our collection.
    with open(real_path(c.get("list-file")), 'r') as file:
        all_post_data = json.load(file)

    if post_data['reddit-id'] in all_post_data:
        print(
            'Reddit ID is identical to one that already exists, for:')
        print(post_data['title'])
        if get_no('Continue? Data will be re-parsed completely. '):
            print(
                'Okay, skipping after attempting to get the discussion data.')
            if not os.path.isfile(get_post_path(reddit_id)):
                print('Discussion did not exist...')
                return None
            with open(get_post_path(reddit_id), 'r') as file:
                return json.load(file)

    # We have two files: One for the discussion text + metadata, another
    # for the comment text + metadata
    # Names are comments_%id.s.json and discussion_%id.s.json
    discussion_filename = get_post_path(reddit_id)
    comments_filename = get_comments_path(reddit_id)

    # Let's put some minimal information in!
    minformation = {
        'Number of Comments': post_data['num-comments'],
        'Submission Score': post_data['score'],
        'Reddit ID': post_data['reddit-id'],
        'Title': post_data['title']
    }

    # Put this information into the file that contains information
    # on all posts, indexed by the reddit ID.
    all_post_data[post_data['reddit-id']] = minformation

    # If we already have a file for the post or the comments
    if os.path.isfile(discussion_filename) or os.path.isfile(comments_filename):
        print('Found a duplicate file. Overwrite?')
        if get_no():
            # Pretty sure this warning is incorrect, we haven't written
            # anything to the filesystem yet.
            print('Warning: Leaving artifact in all_post_data.')
            return None

    # Claim this slot for us! This can't possibly go wrong.
    # Basically, we're writing the updated 'all post information'
    # to the file that contains that information.
    write_pretty_json(
        real_path(c.get("list-file")), all_post_data)

    # We can just go ahead and write out our entire thing now.
    with open(discussion_filename, 'w') as file:
        json.dump(post_data, file, indent=4, sort_keys=True)

    # Parse comments then output to the comments file
    comments = get_comments(s, c=c)
    if comments is not None:
        with open(comments_filename, 'w') as file:
            json.dump(comments, file,
                      indent=4, sort_keys=True)

    c.write("Parsed url(" + str(url) +
            ") or id(" + str(rid) + ")")
    return post_data


def try_get_ids_from_discussion(discussion, c):
    d_text = discussion['fulltext']  # Discussion text

    prev_id = find_prev_post(d_text, c)
    next_id = find_next_post(d_text, c)

    return [RedditID(prev_id), RedditID(next_id)]


"""
what is this for? hm
def try_get_ids_from_id(reddit_id):
    if not os.path.isfile(get_post_path(reddit_id)):
        return None
    with open(get_post_path(reddit_id), 'r') as file:
        return try_get_ids_from_discussion(json.load(file))
"""


def parse_update_discussion(data, c):
    """
    Takes a dictionary, parses it for extra information, and adds it.
    :param data: Dictionary input.
    :return: No return value, uses data as an out param.
    """
    # This is the method to update for any kind of parsing that doesn't require a Reddit API call

    # Gets [prev, next] and converts None to 'None' for JSON output
    # Looking at this now, not sure stringifying None is necessary

    # Get previous/next urls and ids for easier parsing
    # We might need to re-download the full discussion at one point within this method
    # But for now, it's not really necessary
    ids = try_get_ids_from_discussion(data, c)
    data['prev-id'] = str(ids[0].id())
    data['next-id'] = str(ids[1].id())
    # data['prev-url'] = ids[0].url()
    # data['next-url'] = ids[1].url()

    # Get information about the discussion itself
    # TODO - Make this configurable.
    num_mo = re.match(r'.+# *([0-9]+) *:.+', data['title'])
    if num_mo is not None:
        # 1 discussion isn't numbered, 1 is misnumbered (12)
        data['discussion-number'] = int(num_mo.group(1))
    name_mo = re.search(r'[^#:]+: +(.*)', data['title'])
    if name_mo is not None:
        data['subject-name'] = name_mo.group(1)


def parse_update_discussion_from_id(reddit_id, c):
    # Our typical file i/o guard
    if not os.path.isfile(get_post_path(reddit_id)):
        return None
    with open(get_post_path(reddit_id), 'r') as file:
        data = json.load(file)
    parse_update_discussion(data, c)
    write_pretty_json(get_post_path(reddit_id), data)
    return data


def parse_update_discussions(c):
    with open(real_path(c.get("list-file")), 'r') as file:
        all_disc = json.load(file)
    for key in all_disc:
        new_data = parse_update_discussion_from_id(key, c)
        all_disc[key]['Title'] = new_data['title']
    write_pretty_json(
        real_path(c.get("list-file")), all_disc)


def get_referenced_keys(keys):
    referenced = []
    for key in keys:
        with open(get_post_path(key), 'r') as file:
            data = json.load(file)
        if data['next-id'] != 'None':
            referenced.append(data['next-id'])
        if data['prev-id'] != 'None':
            referenced.append(data['prev-id'])
    return referenced


def get_existing_keys(listfile="output/listfile.json"):
    with open(real_path(listfile), 'r') as file:
        all_files = json.load(file)
    return [key for key in all_files]


def check_list(listfile="output/listfile.json"):
    """
    This method takes the full list of files, and returns the post numbers that are missing.
    """
# Get base36 post IDs, which uniquely identify posts & files
    keys = get_existing_keys(listfile)
    post_numbers = []
    number_of_posts = 0
    for key in keys:
        with open(get_post_path(key), 'r') as file:
            disc = json.load(file)
        number_of_posts += 1
        try:
            post_numbers.append(int(disc[Strs.post_number]))
        except KeyError:
            number_of_posts -= 1
            print('Missing post number for id', key)
    post_numbers.sort()
    allnums = [n for n in range(
        post_numbers[0], post_numbers[-1])]
    missingnums = [
        n for n in allnums if n not in post_numbers]
    print('Number of posts found:', number_of_posts)
    return missingnums


def update_posts(c, reddit_instance=None):
    # Generate a reddit instance from the provided configuration.
    if reddit_instance is None:
        reddit_instance = get_reddit_instance(c)
    should_wait = False
    delay = c.get("api-delay", default=30)
    while True:
        if should_wait:
            c.write('Waiting', delay,
                    'seconds for API reasons.')
            time.sleep(delay)
        c.write('Updating temporary post storage.')
        parse_update_discussions(c)
        other_keys = [key for key in get_referenced_keys(
            get_existing_keys()) if key not in get_existing_keys()]
        if len(other_keys) == 0:
            print('All keys are done. Exiting.')
            break
        print('Currently have found', len(other_keys),
              'unparsed keys. Continuing on', str(other_keys[0]) + '...')
        download(
            c=c, rid=other_keys[0], reddit_instance=reddit_instance)
        should_wait = True
        parse_update_discussions(c)


class CommentVisitor:
    def __init__(self):
        self.keys = get_existing_keys()
        self.comments = []

    def next(self):
        if len(self.keys) == 0 and len(self.comments) == 0:
            return None
        if len(self.comments) == 0:
            self.get_more_comments()
            return self.next()
        return self.comments.pop(0)

    def get_more_comments(self):
        if len(self.keys) == 0:
            raise IndexError
        ck = self.keys.pop(0)
        with open(get_comments_path(ck), 'r') as c_file:
            cjd = json.load(c_file)
        for key in cjd:
            self.comments.append(cjd[key])


def generate_index(exceptions_file, index_file="output/index.txt"):
    """
    Generates an index of all posts managed by this app.
    """
    keys = get_existing_keys()
    output = []
    exc = conf(filename=exceptions_file)
    id_exc = conf(source=exc.get("id"))
    case_exc = conf(source=exc.get("case"))
    for key in keys:
        with open(get_post_path(key), 'r') as file:
            disc = json.load(file)

        if 'discussion_number' in disc:
            num = "Character discussion #" + \
                disc['discussion-number']
        else:
            num = case_exc.get("no-discussion-number")

        exception = id_exc.get(key)
        if exception is not None:
            if "number_string" in exception:
                num = exception["number_string"]

        try:
            output.append(
                '[' + num + ': ' + disc['subject-name'] + '](' + disc['permalink'] + ')')
        except KeyError:
            print('Tried to generate index for', key,
                  'but something did not exist!')

    with open(real_path(index_file), 'wb') as file:
        for x in output:
            ax = x.encode('ascii', 'ignore')
            file.write(ax)
            file.write('\n\n'.encode('ascii', 'ignore'))


def main(a):
    c = conf.default()
    c.overwrite(filename="config.json")
    for arg in a:
        mo = re.match(r'^-+([^=]+)$', arg)
        if mo is not None:
            c.overwrite(source={mo.group(1): True})
            continue
        mo = re.match(r'^-+([^=]+)=([^=]+)$', arg)
        if mo is not None:
            c.overwrite(source={mo.group(1): mo.group(2)})
            continue
    c.write('Running with config:')
    c.write(c.dump(indent=2))

    action = None
    if c.get("download-url") is not None:
        action = "download-url"
    if c.get("download-id") is not None:
        action = "download-id"

    if action is None:
        action = c.get("action")
    if action == "update":
        # The list file might not exist.
        list_file = c.get("list-file")
        if not os.path.isfile(real_path(list_file)):
            # Generate list file
            with open(real_path(list_file), "w") as file:
                file.write("{}")
            print("Warning: No posts have been saved. Run downloader.py --download-url=URL with the first post url to get started.")
        index_file = c.get("index-file")
        update_posts(c)
    elif action == "gen-index":
        generate_index(c.get("exceptions-file"))
    elif action == "download-id":
        # The list file might not exist.
        list_file = c.get("list-file")
        if not os.path.isfile(real_path(list_file)):
            # Generate list file
            with open(real_path(list_file), "w") as file:
                file.write("{}")
        download(c=c, rid=c.get("download-id"))


if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
