"""
Provides a simple class for managing configuration.
"""

from os import path
import json

def is_true(val):
    return val == True or str(val).lower() == 'true'

class Configuration:
    def __init__(self, filename=None, source=None):
        self.filename = filename
        self.config = dict()

        # Both a filename and a source can be passed, 
        # the source will be prioritized.
        self.merge(filename=filename, source=source)

    def merge(self, filename=None, source=None):
        # Both a filename and a source can be passed,
        # the source will be merged first.
        if source is not None:
            for k in source:
                if not self.has(k):
                    self.set(k, source[k])
        if filename is not None and path.isfile(filename):
            with open(filename, 'r') as confile:
                try:
                    self.merge(source=json.load(confile))
                except ValueError as e:
                    print(e)

    def overwrite(self, filename=None, source=None):
        # Both a filename and a source can be passed,
        # the source will be used last.
        if filename is not None and path.isfile(filename):
            with open(filename, 'r') as confile:
                try:
                    self.overwrite(source=json.load(confile))
                except ValueError as e:
                    print(e)
        if source is not None:
            for k in source:
                self.set(k, source[k])

    @staticmethod
    def default():
        return Configuration(source={})

    def set(self, key, value):
        self.config[key] = value

    def getAddress(self, list_key, default=None):
        config_mirror = self.config
        for k in list_key[0:-1]:
            if k not in config_mirror:
                return default
            config_mirror = config_mirror[k]
        if list_key[-1] in config_mirror:
            return config_mirror[list_key[-1]]
        return default

    def get(self, key, default=None):
        if isinstance(key, list):
            return self.getAddress(key, default=default)
        return self.config[key] if key in self.config else default
    
    def getStr(self, key, default=None):
        return str(self.get(key, default=default))

    def getTrue(self, key, default=False):
        if key not in self.config:
            return default
        return is_true(self.config[key])

    def write(self, *args, **kwargs):
        if self.getTrue('verbose'):
            print(*args, **kwargs)

    def dump(self, *args, **kwargs):
        return json.dumps(self.config, *args, **kwargs)
